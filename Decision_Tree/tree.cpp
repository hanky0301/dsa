#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#define MAX_FEATURE (1024+1)
using namespace std;

int col = 1;
struct Node
{
	int feature;
	double threshold;
	int label;
	Node* left;
	Node* right;

	Node(): left(NULL), right(NULL){}
};

double confusion(double countYes, double countNo)
{
	double total = countYes + countNo;
	double conf = 1.0 - (countYes / total) * (countYes / total)
					  - (countNo / total) * (countNo / total);
	return conf;
}

double total_confusion(double greaterYes, double greaterNo, 
					   double smallerYes, double smallerNo)
{
	double total = greaterYes + greaterNo + smallerYes + smallerNo;
	double gTotal = greaterYes + greaterNo;
	double sTotal = smallerYes + smallerNo;
	
	return (gTotal / total * confusion(greaterYes, greaterNo) +
			sTotal / total * confusion(smallerYes, smallerNo));
}

void count_decision(vector< vector<double> >& data, double median, int feature, 
					int start, int end,	int& gYes, int& gNo, int& sYes, int& sNo)
{
	gYes = 0;
	gNo = 0; 
	sYes = 0;
	sNo = 0;

	for (int i = start; i < end; i++)
	{
		if (data[i][feature] < median)
		{
			if (data[i][0] == 1)
				sYes++;
			else
				sNo++;
		}
		else
		{
			if (data[i][0] == 1)
				gYes++;
			else
				gNo++;
		}
	}

	return;
}

bool compare(vector<double> vec1, vector<double> vec2)
{
	return (vec1[col] < vec2[col]);
}

int choose_threshold(vector< vector<double> >& data, int count_features,
						int& feature, int start, int end)
{
	int gYes, gNo, sYes, sNo;
	double minConf = 10;
	int idx;

	col = 1;
	for (int i = 1; i <= count_features; i++)
	{
		sort(data.begin() + start, data.begin() + end, compare);
		col++;

		for (int j = start; j < end - 1; j++)
		{
			if (data[j][i] == data[j + 1][i])
				continue;

			double median;
			double conf;
			
			median = (data[j][i] + data[j + 1][i]) / 2.0;
			count_decision(data, median, i, start, end, gYes, gNo, sYes, sNo);
			conf = total_confusion(double(gYes), double(gNo), double(sYes), double(sNo));

			if (conf < minConf)
			{
				idx = j;
				feature = i;
				minConf = conf;
			}
		}		
	}
	return idx;
}

Node* build_tree(vector<vector<double> >& data, 
				int count_features, int start, int end)
{
	int countYes = 0;
	int countNo = 0;
	for (int i = start; i < end; i++)
	{
		if (data[i][0] == 1.0)
			countYes++;
		else
			countNo++;
	}

	if (countNo == 0)
	{
		Node* cur = new Node;
		cur->label = 1;
		cur->label = (countYes > countNo)? 1 : -1;
		return cur;
	}
	if (countYes == 0)
	{ 
		Node* cur = new Node;
		cur->label = -1;
		cur->label = (countYes > countNo)? 1 : -1;
		return cur;
	}
	int i, j;
	for (i = start + 1; i < end; i++)
	{
		for (j = 1; j <= count_features; j++)
			if (data[start][j] != data[i][j])
				break;
		if (j != count_features + 1)
			break;
	}

	if (i == end)
	{
		Node* cur = new Node;
		cur->label = (countYes > countNo)? 1 : -1;
		return cur;
	}

	Node* cur = new Node;
	int feature;
	int low;
	low = choose_threshold(data, count_features, feature, start, end);
	col = feature;
	sort(data.begin() + start, data.begin() + end, compare);

	cur->feature = feature;
	cur->threshold = (data[low][feature] + data[low + 1][feature]) / 2;
	cur->left = build_tree(data, count_features, start, low + 1);
	cur->right = build_tree(data, count_features, low + 1, end);

	return cur;
}

void print_tree(Node* root, FILE* fp)
{
	if (root->left == NULL && root->right == NULL)
	{
		fprintf(fp, "return %d;\n", root->label);
		return;
	}

	fprintf(fp, "if (attr[%d] < %f)\n{\n", root->feature, root->threshold);
	print_tree(root->left, fp);
	fprintf(fp, "}\n");

	fprintf(fp, "else\n{\n");
	print_tree(root->right, fp);
	fprintf(fp, "}\n");
}

int main(int argc,char** argv) 
{
	vector< vector<double> > data;
	int count_features = 0;

	std::ifstream fin;
	string istring;
	fin.open(argv[1]);
	
	while (std::getline(fin, istring))
	{
		char *cstring, *tmp;				
		vector<double> tmpVec(MAX_FEATURE);
		
		cstring = new char[istring.size() + 1];
		strncpy(cstring, istring.c_str(), istring.size()+1);
		
		tmp = strtok(cstring, ": ");
		tmpVec[0] = atof(tmp);
		tmp = strtok(NULL, ": ");
		
		while(tmp != NULL)
		{
			int id = atoi(tmp);

			if (id > count_features)
				count_features = id;

			tmp = strtok(NULL, ": ");
			tmpVec[id] = atof(tmp);	
			tmp = strtok(NULL, ": ");
		}
		data.push_back(tmpVec);
		
	    delete[] cstring;
	}

	Node* root = NULL;
	root = build_tree(data, count_features, 0, data.size());

	FILE* fp = fopen("tree_pred.h", "w");

	fprintf(fp, "int tree_predict(double *attr)\n{\n");
	print_tree(root, fp);
	fprintf(fp, "}\n");

	fclose(fp);

	return 0;
}
