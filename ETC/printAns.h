#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include "record.h"
#include "query.h"
#include "compare.h"
#pragma once
using namespace std;

void possibleRecord(map<string, vector<Record> >& Map, vector<Record>& record, Possible& dataRange)
{
//	Map[dataRange.id];
	vector<Record> possibleRec;
	char tmp[7];
	strcpy(tmp, dataRange.id);
	vector<Record>::iterator lower;
	vector<Record>::iterator upper;
	Record low;
	low.time = dataRange.t1;
	Record up;
	up.time = dataRange.t2;

	for (int i = 0; i < 5; i++)
	{
		tmp[i] = '*';
		for (int j = i + 1; j < 6; j++)
		{
			tmp[j] = '*';
			
			lower = lower_bound(Map[tmp].begin(), Map[tmp].end(), low, compare1);
			upper = upper_bound(Map[tmp].begin(), Map[tmp].end(), up, compare1);
			
			for (vector<Record>::iterator it = lower; it != upper; it++)
				possibleRec.push_back(*it);	
//			for (vector<Record>::iterator it = Map[tmp].begin(); it != Map[tmp].end(); it++)
//				possibleRec.push_back(*it);	
			tmp[j] = dataRange.id[j];
		}
		tmp[i] = dataRange.id[i];
	}

	sort(possibleRec.begin(), possibleRec.end(), compare1);

	//	for (vector<Record>::iterator it = possibleRec.begin(); it != possibleRec.end(); it++)
//		printf("%s %s %d%s\n", it -> orgtime, it -> id, it -> camera, it -> direction);
//	printf("QQQQQ\n");
	vector<Record>::iterator iter;
	iter = unique(possibleRec.begin(), possibleRec.end(), compare);
//	possibleRec.resize(distance(possibleRec.begin(), iter));
//	for (vector<Record>::iterator it = possibleRec.begin(); it != possibleRec.end(); it++)
//		printf("%s %s %d%s\n", it -> orgtime, it -> id, it -> camera, it -> direction);

	for (int i = 0; i < iter - possibleRec.begin(); i++)
		printf("%s %s %d%s\n", possibleRec[i].orgtime, possibleRec[i].id, possibleRec[i].camera, possibleRec[i].direction);		

	printf("--\n");
	return;
/*	for (int i = lower; i <= upper; i++)
	{
		
		countErr = 0;
		countCorr = 0;
		for (int j = 0; j < 6; j++)
		{
			if (countErr == 3)
				break;
			else if (countCorr == 4)
			{
				char tmp[50];
				int len = strlen(record[i].fullRecord) - 1;
				strncpy(tmp, record[i].fullRecord, len);
				tmp[len] = '\0';
				printf("%s %d%s\n", tmp, record[i].camera, record[i].direction);
				break;
			}
			if (record[i].id[j] != dataRange.id[j])
				countErr++;
			else 
				countCorr++;
		}
*/	
}

void anomalyRecord(vector<Record>& record, Anomaly& targetData, int &lower, int& upper)
{
	for (int i = lower + 1; i <= upper; i++)
	{
		int cam;
		if (record[i].direction[0] == 'S')
			cam = 1;
		else
			cam = -1;
		if (!((record[i].camera == record[i - 1].camera + cam && record[i].direction[0] == record[i - 1].direction[0])||
			  (record[i].camera == record[i - 1].camera && record[i].direction[0] != record[i - 1].direction[0])))
		{
			printf("%s %d%s - ", record[i - 1].orgtime, record[i - 1].camera, record[i - 1].direction);
			printf("%s %d%s\n", record[i].orgtime, record[i].camera, record[i].direction);
		}
	}
	printf("--\n");
	return;
}
