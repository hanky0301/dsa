#include <stdio.h>
#include <string.h>
#include "convertTime.h"
#pragma once

class Possible
{
	public:
		Possible(const char source[]);

		char id[7];
		long long t1;
		long long t2;
};

class Anomaly
{
	public:
		Anomaly(const char source[]);
		char id[7];
		
};

Possible::Possible(const char source[])
{
	strncpy(id, source + 9, 6);
	t1 = convertTime(source, 17);
	t2 = convertTime(source, 39);
	return;
}

Anomaly::Anomaly(const char source[])
{
	strncpy(id, source + 8, 6);
}
