#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#pragma once

long long convertTime(const char source[], int pre)
{
	long long time = 0;
	char tmp[6][5] = {{0}};
	
	strncpy(tmp[0], source + pre, 4);
	time |= (atoll(tmp[0]) << 36);

	strncpy(tmp[1], source + pre + 5, 2);
	time |= (atoll(tmp[1]) << 32);
	
	strncpy(tmp[2], source + pre + 8, 2);
	time |= (atoll(tmp[2]) << 24);

	strncpy(tmp[3], source + pre + 11, 2);
	time |= (atoll(tmp[3]) << 16);

	strncpy(tmp[4], source + pre + 14, 2);
	time |= (atoll(tmp[4]) << 8);
//	printf("year = %016llx ", time);

	strncpy(tmp[5], source + pre + 17, 2);
	time |= (atoll(tmp[5]));
//	printf("year = %016llx ", time);

	return time;
}
