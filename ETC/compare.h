#include <stdio.h>
#include <string.h>
#include "record.h"
#pragma once

bool compare1(const Record& r1, const Record& r2)
{
	return (r1.time < r2.time);
}

bool compare2(const Record& r1, const Record& r2)
{
	if (strcmp(r1.id, r2.id) == 0)
		return (r1.time < r2.time);
	else if (strcmp(r1.id, r2.id) < 0)
		return true;
	else if (strcmp(r1.id, r2.id) > 0)
		return false;
}

bool compare(const Record& r1, const Record& r2)
{
	return (r1.time == r2.time);
}
