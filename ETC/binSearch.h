#include <stdio.h>
#include <vector>
#include <string.h>
#include "record.h"
#pragma once
using namespace std;

int binSearch1(vector<Record>& record, long long t, int flag)
{
	int left = 0;
	int right = record.size();
	int mid;
	while (left < right)
	{
		mid = (left + right) / 2;
		if (record[mid].time == t)
			return mid;
		else if (record[mid].time > t)
			right = mid;
		else if (record[mid].time < t)
			left = mid + 1;
	}
	if (flag == 1)
		return left;
	else
		return left - 1;
}

int binSearch2(vector<Record>& record, char targetID[], int flag)
{
//	printf("targetID = %s\n", targetID);
	int len = record.size();
//	printf("len = %d\n", len);
	int left = 0;
	int right = len;
	int mid;
	while (left < right)
	{
		mid = (left + right) / 2;
		if (strcmp(record[mid].id, targetID) == 0)
		{
			if (flag == 1)
				if (mid == 0)
					return mid;
				else if (strcmp(record[mid].id, record[mid - 1].id) != 0)
					return mid;
				else
					right = mid;				
			else
				if (mid == len)
					return mid;
				else if (strcmp(record[mid].id, record[mid + 1].id) != 0)
					return mid;
				else
					left = mid + 1;
		}
		else if (strcmp(record[mid].id, targetID) > 0)
			right = mid;
		else if (strcmp(record[mid].id, targetID) < 0)
			left = mid + 1;
	}
	return -1;
}
