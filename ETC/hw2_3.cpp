#include <stdio.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <map>
#include <assert.h>
#include "record.h"
#include "query.h"
#include "compare.h"
#include "binSearch.h"
#include "printAns.h"
using namespace std;

void constructFilename(char filename[201][50], const char dir[], const char direction[2][3])
{
	int count = 0;
	for (int i = 0; i < 2; i++)
		for (int j = 1; j <= 100; j++)
		{
			snprintf(filename[count], sizeof(filename[0]), "%s%d%s.txt", dir, j, direction[i]);
			count++;
		}
	snprintf(filename[200], sizeof(filename[0]), "%squery.txt", dir);
	return;
}

int main(int argc, char *argv[])
{
	char filename[201][50];
	FILE* fp[201];
	char source[100];
	vector<Record> record1;
	vector<Record> record2;
	char direction[2][3] = {"NS", "SN"};

	constructFilename(filename, argv[1], direction);
	for (int i = 0; i < 201; i++)
	{
		fp[i] = fopen(filename[i], "r");
		assert(fp[i] != NULL);
	}
	
	for (int i = 0; i < 200; i++)
	{
		while (fgets(source, 100, fp[i]) != NULL)
		{
			if (i < 100)
			{
				record1.push_back(Record(source, i + 1, direction[0]));
				record2.push_back(Record(source, i + 1, direction[0]));
			}
			else
			{
				record1.push_back(Record(source, i - 100 + 1, direction[1]));
				record2.push_back(Record(source, i - 100 + 1, direction[1]));
			}
		}			
	}

/*	for (vector<Record>::iterator it = record1.begin(); it != record1.end(); it++)
	{
		printf("fullRec = %s", it -> fullRecord);
		printf("time = %016llx\n", it -> time);
		printf("cam = %d ", it -> camera);
		printf("dir = %s\n", it -> direction);
	}
*/	sort(record1.begin(), record1.end(), compare1);
//	printf("record1.size = %d\n", record1.size());
/*	for (vector<Record>::iterator it = record1.begin(); it != record1.end(); it++)
	{
		printf("fullRec = %s", it -> fullRecord);
		printf("time = %016llx\n", it -> time);
		printf("id = %s ", it -> id);
		printf("cam = %d ", it -> camera);
		printf("dir = %s\n", it -> direction);
	}
*/
//	printf("QQ\n");
	sort(record2.begin(), record2.end(), compare2);
//	printf("record2.size = %d\n", record2.size());
/*	for (int i = 0; i < record2.size(); i++)
	{
		printf("orgt = %s ", record2[i].orgtime);
		printf("id = %s\n", record2[i].id);
		printf("time = %016llx ", record2[i].time);
		printf("cam = %d ", record2[i].camera);
		printf("dir = %s\n", record2[i].direction);
	}
*/
//	printf("qq\n");
	map<string, vector<Record> > Map;
	int size = record1.size();
	char tmp[7];
	for (int i = 0; i < size; i++)
	{
		strcpy(tmp, record1[i].id);
		for (int j = 0; j < 5; j++)
		{
			tmp[j] = '*';
			for (int k = j + 1; k < 6; k++)
			{
				tmp[k] = '*';
			//	printf("%s ", tmp);
				Map[tmp].push_back(record1[i]);
				tmp[k] = record1[i].id[k];
			}
			tmp[j] = record1[i].id[j];
		}
	//	printf("\n");
	}

	int lower, upper;
	while (fgets(source, 100, fp[200]) != NULL)
	{
		if (source[0] == 'p')
		{
			Possible dataRange(source);
		//	printf("%s", source);
		//	printf("%016llx ", dataRange.t1);
		//	printf("%016llx\n", dataRange.t2);
		//	lower = binSearch1(record1, dataRange.t1, 1);
		//	upper = binSearch1(record1, dataRange.t2, 0);
		//	printf("lower = %d\n", lower);
		//	printf("upper = %d\n", upper);
			possibleRecord(Map, record1, dataRange);
		}
		else if (source[0] == 'a')
		{
			Anomaly targetData(source);
		//	printf("%s", source);
		//	printf("id = %s\n", )
			lower = binSearch2(record2, targetData.id, 1);
			upper = binSearch2(record2, targetData.id, 0);
		//	printf("lower = %d\n", lower);
		//	printf("upper = %d\n", upper);
			anomalyRecord(record2, targetData, lower, upper);
		}			
	}

	for (int i = 0; i < 201; i++)
		fclose(fp[i]);
	return 0;
}
