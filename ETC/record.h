#include <stdio.h>
#include <string.h>
#include "convertTime.h"
#pragma once

class Record
{
	public:
		Record(char source[], int cam, char dir[]);
		Record();
		
		int index;
		char orgtime[20];
		long long time;
		char id[7];
		int camera;
		char direction[3];
};

Record::Record(char source[], int cam, char dir[])
{
	strncpy(orgtime, source, 19);
	orgtime[19] = '\0';
	time = convertTime(source, 0);
	strncpy(id, source + 20, 6);
	id[6] = '\0';
	camera = cam;
	strcpy(direction, dir);

	return;
}

Record::Record()
{
}
