## Grading Criteria
#### *git* grading criteria:

- **-1** `strange commit message`

    *All* commit messages are strange

- **-2** `indenting and modifying mixed`

    If code reindentation and modification are mixed in one commit;

- **-2** `\n to \r\n and modifying mixed`

    If changing newline style (\n to \r\n) and modification are mixed in one commit;

- **-3** `no version control`

    If no version control is used (i.e. submitting all codes including the
    provided ones in the **only** commit. If there are multiple commit,
    then that's ok);

#### message explanation

- **make returned non-zero status**: `make` returned non-zero status.
- **correct**: passed
- **incorrect**: the output instruction sequence generated incorrect result
- **runtime error (hw3_1)**: Your program `hw3_1` crashed.
- **runtime error (hw3_2)**: Your program `hw3_2` crashed.
- **runtime error (hw3_2\*)**: The generated instruction crashed
    the reference program `hw3_2`. Empty output will also result in this.
- **killed**: Your program run too long and had been terminated.

#### test data:

- `hw3_1`

test  | content | # of tests | score per test
:----:|:----------------------------:|--:|--:
0 | `<=`, `+`, `-`, `*`, `/` | 15 | 0.67
1 | `<=`, `+`, `-`, `*`, `/` | 5 | 2.00
2 | `<=`, `+`, `-`, `*`, `/` | 5 | 2.00
3 | all operators; no parentheses | 7 | 1.43
4 | all operators; no parentheses | 12 | 0.83
5 | all operators; no parentheses | 6 | 1.67
6 | all except topexpr | 12 | 0.83
7 | all except topexpr | 10 | 1.00
8 | all | 6 | 1.67
8 | all | 7 | 1.43

- `hw3_2`

passed test(s) | score | -
:-----|:--:|:-----------:
7     |  7 |
7,8   | 14 |
7,8,9 | 20 | (all tests)


## Grading Result
### *git* Usage
- Score: 10 

### `hw3_1` Status
- Compilation Status: ok

- Score: 100.00 

- Test 0 

Test | Result | Score 
:-:|:-:|:-:
12-basic-prim.t | correct | 0.67
07-basic-prim.t | correct | 0.67
06-basic-prim.t | correct | 0.67
10-basic-prim.t | correct | 0.67
01-basic-prim.t | correct | 0.67
14-basic-prim.t | correct | 0.67
00-basic-prim.t | correct | 0.67
09-basic-prim.t | correct | 0.67
13-basic-prim.t | correct | 0.67
05-basic-prim.t | correct | 0.67
08-basic-prim.t | correct | 0.67
02-basic-prim.t | correct | 0.67
03-basic-prim.t | correct | 0.67
11-basic-prim.t | correct | 0.67
04-basic-prim.t | correct | 0.67

- Test 1 

Test | Result | Score 
:-:|:-:|:-:
15-mixed-no-assoc.t | correct | 2.00
19-mixed-no-assoc.t | correct | 2.00
16-mixed-no-assoc.t | correct | 2.00
18-mixed-no-assoc.t | correct | 2.00
17-mixed-no-assoc.t | correct | 2.00

- Test 2 

Test | Result | Score 
:-:|:-:|:-:
22-mixed-all.t | correct | 2.00
20-mixed-all.t | correct | 2.00
21-mixed-all.t | correct | 2.00
23-mixed-all.t | correct | 2.00
24-mixed-all.t | correct | 2.00

- Test 3 

Test | Result | Score 
:-:|:-:|:-:
31-pow-assoc.t | correct | 1.43
25-pow-prim.t | correct | 1.43
29-pow-prim.t | correct | 1.43
27-pow-prim.t | correct | 1.43
26-pow-prim.t | correct | 1.43
30-pow-prim.t | correct | 1.43
28-pow-prim.t | correct | 1.43

- Test 4 

Test | Result | Score 
:-:|:-:|:-:
41-prior.t | correct | 0.83
40-prior.t | correct | 0.83
32-prior.t | correct | 0.83
39-prior.t | correct | 0.83
38-prior.t | correct | 0.83
34-prior.t | correct | 0.83
36-prior.t | correct | 0.83
37-prior.t | correct | 0.83
42-prior.t | correct | 0.83
43-prior.t | correct | 0.83
35-prior.t | correct | 0.83
33-prior.t | correct | 0.83

- Test 5 

Test | Result | Score 
:-:|:-:|:-:
45-mixed-pow.t | correct | 1.67
49-mixed-pow.t | correct | 1.67
44-mixed-allop.t | correct | 1.67
47-mixed-pow.t | correct | 1.67
46-mixed-pow.t | correct | 1.67
48-mixed-pow.t | correct | 1.67

- Test 6 

Test | Result | Score 
:-:|:-:|:-:
59-prior.t | correct | 0.83
53-prior.t | correct | 0.83
51-prior.t | correct | 0.83
61-prior.t | correct | 0.83
54-prior.t | correct | 0.83
57-prior.t | correct | 0.83
58-prior.t | correct | 0.83
55-prior.t | correct | 0.83
60-prior.t | correct | 0.83
52-prior.t | correct | 0.83
56-prior.t | correct | 0.83
50-prior.t | correct | 0.83

- Test 7 

Test | Result | Score 
:-:|:-:|:-:
65-expr.t | correct | 1.00
66-expr.t | correct | 1.00
62-expr.t | correct | 1.00
67-expr.t | correct | 1.00
70-expr.t | correct | 1.00
69-expr.t | correct | 1.00
64-expr.t | correct | 1.00
63-expr.t | correct | 1.00
68-expr.t | correct | 1.00
71-expr.t | correct | 1.00

- Test 8 

Test | Result | Score 
:-:|:-:|:-:
75-if.t | correct | 1.67
76-basic-sample.t | correct | 1.67
73-basic-sample.t | correct | 1.67
77-basic-sample.t | correct | 1.67
72-basic-sample.t | correct | 1.67
74-if.t | correct | 1.67

- Test 9 

Test | Result | Score 
:-:|:-:|:-:
83-sum.t | correct | 1.43
84-compose.t | correct | 1.43
82-fib.t | correct | 1.43
81-factorial.t | correct | 1.43
79-ap-assoc.t | correct | 1.43
78-ap-assoc.t | correct | 1.43
80-ap-prior.t | correct | 1.43

### `hw3_2` Status
- Compilation Status: hw3_2 not found 

