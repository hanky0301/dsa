#include <stdio.h>
#include <stdlib.h>
#include "bst.h"
#include "avl.h"
#include "rb.h"

void preorder_string_bst(const struct bst_node *node)
{
	if (node == NULL)
		return;
	printf("%s ", *((char**)node->bst_data));
	if (node->bst_link[0] != NULL || node->bst_link[1] != NULL)
	{
		putchar('(');
		preorder_string_bst(node->bst_link[0]);
		putchar(',');
		putchar(' ');
		preorder_string_bst(node->bst_link[1]);
		putchar(')');
	}
}

void preorder_string_avl(const struct avl_node *node)
{
	if (node == NULL)
		return;
	printf("%s ", *((char**)node->avl_data));
	if (node->avl_link[0] != NULL || node->avl_link[1] != NULL)
	{
		putchar('(');
		preorder_string_avl(node->avl_link[0]);
		putchar(',');
		putchar(' ');
		preorder_string_avl(node->avl_link[1]);
		putchar(')');
	}
}

void preorder_string_rb(const struct rb_node *node)
{
	if (node == NULL)
		return;
	printf("%s ", *((char**)node->rb_data));
	if (node->rb_link[0] != NULL || node->rb_link[1] != NULL)
	{
		putchar('(');
		preorder_string_rb(node->rb_link[0]);
		putchar(',');
		putchar(' ');
		preorder_string_rb(node->rb_link[1]);
		putchar(')');
	}
}

int str_compare(const void* pa, const void* pb, void* param)
{
	return strcmp(*((char**) pa), *((char**) pb));
}

int main()
{
	struct bst_table *bstTree;
	struct avl_table *avlTree;
	struct rb_table *rbTree;
	bstTree = bst_create(str_compare, NULL, NULL);
	avlTree = avl_create(str_compare, NULL, NULL);
	rbTree = rb_create(str_compare, NULL, NULL);

	char name[32][10] = {
		"Chuang", "Chou", "Chang", "Chao", "Chen",	"Cheng", 
		"Chu", "Fu", "Fuh", "Hsiang", "Hsu", "Hsueh", "Hung", 
		"Jang", "Kao", "Kuo", "Lai", "Lee", "Liao", "Lin", 
		"Liou", "Liu", "Lu", "Lyuu", "Ouhyoung", "Oyang", 
		"Shih", "Tsai", "Tseng", "Wang", "Wu", "Yang"
	};

	int i;
	for (i = 0; i < 32; i++)
	{
		char** element = (char**)malloc(sizeof(char*));
		*element = name[i];
		void **p1 = bst_probe(bstTree, element);
		void **p2 = avl_probe(avlTree, element);
		void **p3 = rb_probe(rbTree, element);
	}

	preorder_string_bst(bstTree->bst_root);
	puts("");
	
	preorder_string_avl(avlTree->avl_root);
	puts("");

	preorder_string_rb(rbTree->rb_root);
	puts("");
	
	return 0;
}
