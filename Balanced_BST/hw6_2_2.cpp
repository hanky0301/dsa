#include <cstdio>
#include <cstdlib>
#include <cstring>
extern "C" 
{ 
#include "avl_ntudsa.h" 
}

struct Student
{
	char id[20];
	int score[5];
	int average;

	Student(char i[20], int s[5])
	{
		strcpy(id, i);
		for (int i = 0; i < 5; i++)
			score[i] = s[i];

		int sum = 0;
		for (int i = 0; i < 5; i++)
			sum += score[i];
		average = sum / 5;
	}
};

int compare(const void* pa, const void* pb, void* param)
{
	Student* ptr1 = (Student*)pa;
	Student* ptr2 = (Student*)pb;

	if (ptr1->average < ptr2->average)
		return -1;
	if (ptr1->average > ptr2->average)
		return 1;
	if (ptr1->score[1] < ptr2->score[1])
		return -1;
	if (ptr1->score[1] > ptr2->score[1])
		return 1;
	if (ptr1->score[2] < ptr2->score[2])
		return -1;
	if (ptr1->score[2] > ptr2->score[2])
		return 1;
	if (ptr1->score[4] < ptr2->score[4])
		return -1;
	if (ptr1->score[4] > ptr2->score[4])
		return 1;
	if (ptr1->score[3] < ptr2->score[3])
		return -1;
	if (ptr1->score[3] > ptr2->score[3])
		return 1;
	if (ptr1->score[0] < ptr2->score[0])
		return -1;
	if (ptr1->score[0] > ptr2->score[0])
		return 1;
	return -strcmp(ptr1->id, ptr2->id);	
}

struct avl_node* search(int rank, struct avl_node* root, int& count)
{
	int curr = count + root->avl_cnode[1];

	if (curr == rank - 1)
		return root;
	else if (curr >= rank)
		return search(rank, root->avl_link[1], count);
	else
	{
		count += (1 + root->avl_cnode[1]);
		return search(rank, root->avl_link[0], count);
	}
}

void preorder_string_avl(const struct avl_node *node)
{
	if (node == NULL)
		return;
	printf("%s ", ((Student*)node->avl_data)->id);
	if (node->avl_link[0] != NULL || node->avl_link[1] != NULL)
	{
		putchar('(');
		preorder_string_avl(node->avl_link[0]);
		putchar(',');
		putchar(' ');
		preorder_string_avl(node->avl_link[1]);
		putchar(')');
	}
}

int main()
{
	char oper[10];
	struct avl_table *tree;
	tree = avl_create(compare, NULL, NULL);
	while(scanf("%s", oper) != EOF)
	{
		if (oper[0] == 'p')
		{
			char id[20];
			int s[5];
			scanf("%s", id);
			for (int i = 0; i < 5; i++)
				scanf("%d", &s[i]);

			Student* elem = new Student(id, s);
			void **p = avl_probe(tree, elem);
		}
		else if (oper[0] == 's')
		{
			int rank;
			scanf("%d", &rank);
			int count = 0;
			Student* stu = 
			  (Student*) search(rank, tree->avl_root, count)
			  ->avl_data;
			printf("%s\n", stu->id);
		}
		else
		{
			preorder_string_avl(tree->avl_root);
			puts("");
		}
	}
	return 0;
}
