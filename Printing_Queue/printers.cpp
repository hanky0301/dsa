#include <stdio.h>
#include <vector>
#include <cstring>
#include "leftist_tree.h"
using namespace std;

void execute_command(vector<MaxLeftTree>& printer)
{
	char oper[10];
	scanf("%s", oper);

	if (oper[0] == 'a')
	{
		int j_id, pri, p_id;
		scanf("%d %d %d", &j_id, &pri, &p_id);

		Job* job = new Job;
		job->job_id = j_id;
		job->priority = pri;
		job->shortest = 1;
		
		printer[p_id].insert(job);
		printf("%d jobs waiting on printer %d\n",
				printer[p_id].getSize(), p_id);
	}
	else if (oper[0] == 'p')
	{
		int p_id;
		scanf("%d", &p_id);
		if (printer[p_id].isEmpty())
			printf("no documents in queue\n");
		else
		{
			printf("%d printed\n", printer[p_id].getMax()->job_id);
			printer[p_id].removeMax();
		}
	}
	else
	{
		int p1_id, p2_id;
		scanf("%d %d", &p1_id, &p2_id);
		printer[p2_id].merge(printer[p1_id]);
		printf("%d jobs waiting on printer %d after moving\n", 
				printer[p2_id].getSize(), p2_id);
	}
	return;
}

int main()
{
	int count_printers = 0;
	int count_commands = 0;

	scanf("%d %d", &count_printers, &count_commands);
	vector<MaxLeftTree> printer(count_printers);

	for (int i = 0; i < count_commands; i++)
		execute_command(printer);

	return 0;
}
