#pragma once
#include <cstdio>

struct Job
{
	int job_id;
	int priority;
	int shortest;
	Job* left;
	Job* right;
};

class MaxLeftTree
{
public:
	MaxLeftTree(): root(NULL), size(0) {}
	MaxLeftTree(Job* r): root(r), size(1) {}

	int getSize() const
	  { return size; }
	bool isEmpty() const
	  { return (root == NULL); }
	Job* getMax() const
	  { return root; }

	void merge(MaxLeftTree& leftTree);
	Job* meld(Job* job1, Job* job2);

	void insert(Job* job);
	void removeMax();

private:
	int size;
	Job* root;
//	int shortest(Job* job) const;
};
