#include "leftist_tree.h"
#include <cstdio>
#include <algorithm>
using namespace std;

void MaxLeftTree::merge(MaxLeftTree& leftTree)
{
	size += leftTree.size;
	root = meld(root, leftTree.root);
	leftTree.root = NULL;
	leftTree.size = 0;
}

Job* MaxLeftTree::meld(Job* root1, Job* root2)
{
	if (root1 == NULL)
		return root2;
	if (root2 == NULL)
		return root1;

	if (root1->priority < root2->priority)
	{
		Job* tmp = root1;
		root1 = root2;
		root2 = tmp;
	}	
	root1->right = meld(root1->right, root2);

	if (root1->left == NULL)
	{
		root1->left = root1->right;
		root1->right = NULL;
	}
	else 
	{
		if (root1->left->shortest < root1->right->shortest)
		{
			Job* tmp = root1->left;
			root1->left = root1->right;
			root1->right = tmp;
		}
		root1->shortest = root1->right->shortest + 1;
	}

	return root1;
}

// insert(MaxLeftTree& leftTree)
void MaxLeftTree::insert(Job* job)
{
	MaxLeftTree toInsert(job);
	this->merge(toInsert);
}

// void removeMax();
void MaxLeftTree::removeMax()
{
	root = meld(root->left, root->right);
	size--;
}
