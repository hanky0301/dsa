#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <map>
#include <vector>
#include <string>
#include <utility>
#include <list>

#include "rbdic.h"
#include "command.h"
using namespace std;

//Main structure
Dictionary_t _content, _from, _to;
map<int, long long int> _date_table;  //id -> date
map<int, Relate_t*> _id_table;  //id -> related things   //Can't fons a way to put this in reality.
//////yeah, I know these are blobal variables.

////////////////////////////////////////////////////////////
void initialization(void){
	for(int i=0 ; i<36 ; i++){
		_content.link[i]=NULL;
		_from.link[i]=NULL;
		_to.link[i]=NULL;
	}
	_content.possible = new map<int,int>;
	_from.possible = new map<int,int>;
	_to.possible = new map<int,int>;
return;
}
////////////////////////////////////////////////////////////
//
void dic_init(Dictionary_t* q){
	for(int i=0 ; i<36 ; i++)
		q->link[i]=NULL;
	q->possible = new map<int,int>;
return;
}
void relate_init(Relate_t* q){  
	q->content = new list<Dictionary_t*>;
	q->from = new list<Dictionary_t*>;
	q->to = new list<Dictionary_t*>;
return;
}
//merge two ordered vector<int>
/*
void merge_map(map<int,int>& re, list<int>& q){
	list<int>::iterator j=q.begin(), je=q.end();
	for(j ; j!=je ; j++)	
		re.find(*j);
return;
}*/

/////////////////////////////////////////////////////////

//add into dictionary tree
//map<int,int>::iterator _add_in_dic(Dictionary_t *dic_tree, char* tmp, int id){ 
Dictionary_t* _add_in_dic(Dictionary_t *dic_tree, char* tmp, int id){
	Dictionary_t *ptr = dic_tree;
	int tmpi, length = (signed)strlen(tmp);
	//cout<<tmp<<endl;
	for(int i=0 ; i<length ; i++){
		if(isalpha(tmp[i])) tmpi = tolower(tmp[i]) - 'a';
		else tmpi = tmp[i] - '0' + 26;

		if(ptr->link[tmpi]==NULL){
			Dictionary_t *newnode = (Dictionary_t*)malloc(sizeof(Dictionary_t));	
			dic_init(newnode);
			ptr->link[tmpi] = newnode;
			ptr = newnode;
		}
		else
			ptr= ptr->link[tmpi];
	}
	ptr->possible->insert(pair<int, int>(id, id));
	//pair<map<int,int>::iterator,bool> it = ptr->possible.insert(pair<int, int>(id, id));
//return it.first;
return ptr;
}
//check from dictionary tree
list<int> check_dic(Dictionary_t *dic_tree, string& target){
	Dictionary_t *ptr = dic_tree;
	list<int> re;
	int tmpi, length = target.length();
	//cout<<target<<endl;
	for(int i=0 ; i<length ; i++){
		if(isalpha(target[i])) tmpi = tolower(target[i]) - 'a';
		else tmpi = target[i] - '0' + 26;
		if(ptr->link[tmpi] == NULL){
			return re;
		}else{
			ptr = ptr->link[tmpi];
		}
	}
	map<int,int>::iterator end = ptr->possible->end();
	for(map<int,int>::iterator it = ptr->possible->begin() ; it!=end ; it++){
		re.push_back(it->second);
	}
return re;
}
///////////////////////////////////////////////////////////
int add_mail(string q){
	//cout<<"EEEEEEEEEEEE"<<endl;
	char path[500];
	strcpy(path, q.c_str());
	FILE *mail = fopen(path, "r");
	//if(mail == NULL){cout<<"**ERROR** file open failed"<<endl;return -1;}
	
	char from[100];
	char tmp[100];
	int day, year, hour, min;
	char month[100];
	long long int date;
	int id;
	char subject[5000];
	char to[100];
	//new a mail
	Relate_t *newrelate = (Relate_t*)malloc(sizeof(Relate_t));
	relate_init(newrelate);  	
	//
	//Read header;
	fscanf(mail, "%s", tmp); //Eat from:
	fscanf(mail, "%s", from);
	fscanf(mail, "%s", tmp); //eat date:
	fscanf(mail, "%d%s%d%s%d:%d", &day, month, &year, tmp, &hour, &min);
	fscanf(mail, "%s", tmp);  //eat Message-ID:
	fscanf(mail, "%d", &id);
	if( _id_table.find(id) != _id_table.end()){   //Check is already exist
		fclose(mail);
		return -1;
	}
	fscanf(mail, "%s", tmp); //eat Subject:
	fgets(subject, 5000, mail);
	fscanf(mail, "%s", tmp); //eat To:
	fscanf(mail, "%s", to);

	//
	///*adding header:*//////////////


	//add from 
	//newrelate->from = _add_in_dic(&_from, from, id); 
	newrelate->from->push_back( _add_in_dic(&_from, from, id) );

	//add to
	//newrelate->to = _add_in_dic(&_to, to, id);  
	newrelate->to->push_back( _add_in_dic(&_to, to, id) );

	//add date
	newrelate->date = date = datetoll(year, month, day, hour, min);
	date = datetoll(year, month, day, hour, min);
	_date_table[id] = date;
	//pair<map<int,int>::iterator,bool> it = newrelate->date_loc->second.insert(pair<int,int>(id, id));
	//newrelate->date = it.first;
	
	//add subject in content
	char *s=subject;  //act like strtok, just change the condition of slicing.
	int tmplen = strlen(subject);
	for(int i=0 ; i<tmplen ; i++){
		if(not isalnum(subject[i])){
			subject[i]='\0';
			if(strlen(s)!=0){
				//cout<<"\""<<s<<"\""<<endl;
				newrelate->content->push_back( _add_in_dic(&_content, s, id) );
				//newrelate->content.push_back( _add_in_dic(&_content, s, id));
			}
			s = subject+i+1;
		}
	}
	
	//add content until EOF
	fscanf(mail, "%s", tmp);  //eat content:
	int ctr=0;  //counter
	while(fscanf(mail, "%c", tmp+ctr) != EOF){
		if(not isalnum(tmp[ctr])){
			tmp[ctr]='\0';
			ctr=-1;
			if(strlen(tmp)!=0){
				if(tmp == "S" or tmp =="s"){
					cout<<"HDUIFH"<<endl;
				}
				newrelate->content->push_back( _add_in_dic(&_content, tmp, id) );
				//newrelate->content.push_back( _add_in_dic(&_content, tmp, id));
				//cout<<tmp<<endl;;
			}
		}
		++ctr;
	}
	if(ctr!=0){
		tmp[ctr]='\0';
		if(strlen(tmp)!=0){
			newrelate->content->push_back( _add_in_dic(&_content, tmp, id) );
			//newrelate->content.push_back( _add_in_dic(&_content, tmp, id));
			//cout<<tmp<<endl;	
		}
	}
	
	//hook id to nwerelate
	_id_table[id]=newrelate;
	

	fclose(mail);

return _id_table.size();
}
//////////////////////////////////////////////////////////////////
int remove_mail(int id){
	map<int, Relate_t*>::iterator relation = _id_table.find(id);
	if(relation == _id_table.end()){
		return -1;
	}
	while(not relation->second->content->empty()){  //remove content
		relation->second->content->back()->possible->erase(id);
		relation->second->content->pop_back();
	}
	while(not relation->second->from->empty()){  //remove from
		relation->second->from->back()->possible->erase(id);
		relation->second->from->pop_back();
	}
	while(not relation->second->to->empty()){  //remove to
		relation->second->to->back()->possible->erase(id);
		relation->second->to->pop_back();
	}
	_date_table.erase(id);  //remove date
	_id_table.erase(relation);   //remove itself in id table
return _id_table.size();	
}
//////////////////////////
//intersect A&B
list<int> intersect_list(list<int>& q, list<int>& w){
	list<int>::iterator i=q.begin(), j=w.begin(), ie=q.end(), je=w.end();
	list<int> re;
	if(i==ie or j==je) return re;
	while(1){
		if(*i < *j){
			++i;
			if(i == ie) break;
		}else if(*i > *j){
			++j;
			if(j == je) break;
		}else{
			re.push_back(*i);
			++i;++j;
			if(i==ie or j==je) break;
		}
	}
	return re;
}
//union A+B
list<int> union_list(list<int>& q, list<int>& w){
	list<int>::iterator i=q.begin(), j=w.begin(), ie=q.end(), je=w.end();
	list<int> re;
	int F;
	if(i==ie){
		F=1;
	}else if(j==je){
		F=2;
	}else while(1){
		if(*i < *j){
			re.push_back(*i);
			++i;
			if(i==ie){
				F=1;	
				break;
			}
		}else if(*i > *j){
			re.push_back(*j);
			++j;
			if(j==je){
				F=2;
				break;
			}
		}else{
			re.push_back(*i);
			++i;++j;
			if(i==ie){
				F=1;
				break;
			}else if(j==je){
				F=2;
				break;
			}
		}		
	}
	switch(F){
	case 1:
		while(j != je){
			re.push_back(*j);
			++j;
		}
	break;
	case 2:
		while(i != ie){
			re.push_back(*i);
			++i;
		}
	break;
	}
return re;
}
//complement A-B
list<int> complement_list(list<int>& q){
	list<int> re;
	list<int>::iterator i=q.begin(), ie=q.end();
	map<int,Relate_t*>::iterator j=_id_table.begin(), je=_id_table.end();
	int F=0;
	
	if(j==je) return re;
	else if(i==ie) F=1;
	else while(1){
		if(*i < j->first){
			++i;
			if(i == ie){
				F=1;
				break;
			}
		}else if(*i > j->first){
			re.push_back(j->first);
			++j;
			if(j==je)return re;
		}else{
			++i;++j;
			if(j==je){return re;}
			if(i==ie){F=1; break;}
		}
	}
	if(F==1){
		while(j != je){
			re.push_back(j->first);
			++j;
		}
	}
return re;
}
////////////////////////////////////////
list<int> query(Command_t& q){
	list<int> final;
	list<int> re;
	list<int> tmplist;
	list<list<int> > buf;
	
	//from
	if(q._F)	
		re = check_dic(&_from, q._from);
	//cout<<"from done"<<endl;

	//to
	if(q._T){
		tmplist = check_dic(&_to, q._to);
		if(q._F)	
			re = intersect_list(re, tmplist);
		else
			re = tmplist;
	}
	//cout<<"to done"<<endl;

	//content
	list<int> tmp1, tmp2;
	list<string>::iterator end = q._keyword.end();
	for(list<string>::iterator it=q._keyword.begin() ; it!=end ; it++){
		//cout<<*it<<endl<<"**Push any button to comtinue**"<<endl;
		//cin.get();
		//cout<<*it<<endl;
		if(*it == "&"){  
			tmp1 = buf.back(); buf.pop_back();
			tmp2 = buf.back(); buf.pop_back();
			buf.push_back( intersect_list(tmp1, tmp2) );
		}else if(*it == "|"){
			tmp1 = buf.back(); buf.pop_back();
			tmp2 = buf.back(); buf.pop_back();
			buf.push_back( union_list(tmp1, tmp2) ); 
		}else if(*it == "!"){
			tmp1 = buf.back(); buf.pop_back();
			buf.push_back( complement_list(tmp1) );		
		}else{
			buf.push_back( check_dic(&_content, *it) );
		}
	}
	//cout<<"content almost done"<<endl;
	//process all the result and date together
/*
	cout<<"re ";
	for(list<int>::iterator i=re.begin() ; i!=re.end() ; i++)
		cout<<*i;
	cout<<"\ncontent ";
	for(list<int>::iterator i=buf.back().begin() ; i!=buf.back().end() ; i++)
		cout<<*i;
	cout<<endl;
*/
	list<int>::iterator i=buf.back().begin(), ie=buf.back().end();
	int F=0;
	
	if(not q._F and not q._T){
		for( ; i!=ie ; ){
			if(_date_table[*i]<q._date[0] or _date_table[*i]>q._date[1])
				i = buf.back().erase(i);
			else 
				++i;
		}
	}
	else if(i==ie);
	else{
		list<int>::iterator it=re.begin(), end2=re.end();
		while(1){
			if(*it < *i){
				//cout<<"EEEE"<<endl;
				++it;
				if(it == end2){F=1;	break;}
			}else if(*it > *i){
				i = buf.back().erase(i);
				if(i == ie)	break;
			}else{
				if(_date_table[*i]<q._date[0] or _date_table[*i]>q._date[1])
					i = buf.back().erase(i);
				else
					++i;
				++it;
				if( i==ie ) break;
				if( it==end2) {F=1; break;}
			}
		}
		if(F == 1){
			while( i != ie) i = buf.back().erase(i);
		}

	}
	//cout<<"content and date done"<<endl;

return buf.back();
}


