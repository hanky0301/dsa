#ifndef RBDIC
#define RBDIC 2

#include <iostream>
#include <cstdio>
#include <cstdio>
#include <list>
#include <map>
#include <vector>
#include <utility>

#include "command.h"
using namespace std;


typedef struct DICTION{
	struct DICTION *link[36];  //a~z  0~9
	map<int, int> *possible;	
}Dictionary_t;

typedef struct RELATE{   
	list<Dictionary_t*> *content;
	list<Dictionary_t*> *from;
	list<Dictionary_t*> *to;
	long long int date;  
	
}Relate_t;

//map<long long int, map<int, int> > date_table;   //date -> Data_t
//map<int, Relate_t*> id_table;     //id -> related things (needed by "remove")  //Not used.

void initialization(void);
int add_mail(string);
int remove_mail(int);
list<int> query(Command_t&);


#endif
