#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <functional>
#include <stack>
#include <algorithm>
#include <iostream>
#include "command.h"
using namespace std;

unordered_map<string, long long> month_map({
	{"January", 1}, {"February", 2}, {"March", 3},
	{"April", 4}, {"May", 5}, {"June", 6},
	{"July", 7}, {"August", 8}, {"September", 9},
	{"October", 10}, {"November", 11}, {"December", 12}
});
struct Sr_data // stores sender and receiver
{
	string sender;
	string receiver;

	bool operator==(const Sr_data& sr) const
	{
		return (sender == sr.sender && receiver == sr.receiver);
	}
};
struct Mail_data
{
	Sr_data sr;
	long long date;
	unordered_set<string> keyword;
};
struct IDandDate // stores id and date, serves as the search tool
{
	int id;
	long long date;
	
	bool operator<(const IDandDate& id_n_date) const
	{
		return (date < id_n_date.date || (date == id_n_date.date && id < id_n_date.id));
	}
};
namespace std
{
	template<>
	struct hash<Sr_data>
	{
		size_t operator()(const Sr_data& sr) const
		{
			return (hash<string>()(sr.sender) ^ (hash<string>()(sr.receiver) << 1));
		}
	};
}

Mail_data database[10010];
int mail_status[10010] = {0};
int count_mail = 0;
unordered_map<Sr_data, set<IDandDate> > Map;


// to be optimized
void parse(char* raw, int id)
{
	int len = strlen(raw);
	char* cur = raw;
	int count = 0;
	
	for (int i = 0; i <= len; i++)
	{
		count++;
		if (i != len)
			raw[i] = tolower(raw[i]);
		if (!isalnum(raw[i]))
		{
			string word(cur, cur + count - 1); // try to eliminate this step
			if (word.size() != 0)
			{
				// cout << word << " ";
				database[id].keyword.insert(word);
			}
			cur += count;
			count = 0;
		}
	}
	return;
}

// to be optimized
void convert_date(int year, char* month, int day, char* time, long long& date)
{
	long long hr = (time[0] - '0') * 10 + (time[1] - '0');
	long long min = (time[3] - '0') * 10 + (time[4] - '0');

	date = 0;
	date += min;
	date += hr * 100;
	date += (long long)day * 10000;
	date += month_map[month] * 1000000;
	date += (long long)year * 100000000;

	return;
}

int add(char* filename)
{
	int mail_id;
	int k = strlen(filename) - 1;
	while(isdigit(filename[k]) == true)
		k--;
	mail_id = atoi(filename + k + 1);
	if (mail_status[mail_id] == 1)
		return -1;

	if (mail_status[mail_id] == 0)
	{
		FILE* fp = fopen(filename, "r");
		assert(fp != NULL);

		char tmp[15];
		int id;
		char s[55];
		int day, year;
		char month[15]; 
		char time[10];
		char r[55];
		char cats[10];
		char catc[10];
		char subject[110];
		char content[500];

		fscanf(fp, "%s%s", tmp, s);
		fscanf(fp, "%s", tmp);
		fscanf(fp, "%d%s%d%s%s", &day, month, &year, tmp, time);
		fscanf(fp, "%s%d", tmp, &id);
		fscanf(fp, "%s ", tmp);
		fgets(subject, 110, fp);
		fscanf(fp, "%s%s", tmp, r);
		fscanf(fp, "%s\n", tmp);
		parse(subject, id);
		while (fscanf(fp, "%s", content) != EOF)
			parse(content, id);

		string sender(s, s + strlen(s));   // try to eliminate this step
		string receiver(r, r + strlen(r)); // try to eliminate this step
		transform(sender.begin(), sender.end(), sender.begin(), ::tolower);
		transform(receiver.begin(), receiver.end(), receiver.begin(), ::tolower);
		convert_date(year, month, day, time, database[id].date);
		database[id].sr = {sender, receiver};
		fclose(fp);
	}

	Sr_data sr[4] = { {database[mail_id].sr.sender, database[mail_id].sr.receiver}, {"*", "*"},
					  {"*", database[mail_id].sr.receiver}, {database[mail_id].sr.sender, "*"} };
	for (int i = 0; i < 4; i++)
		Map[sr[i]].insert({mail_id, database[mail_id].date});

	mail_status[mail_id] = 1;
	count_mail++;
	return count_mail;
}

int remove(int id)
{
	if (id < 0 or id > 10000)
		return -1;
	if (mail_status[id] != 1)
		return -1;
	Sr_data sr[4] = { {database[id].sr.sender, database[id].sr.receiver}, {"*", "*"},
					  {"*", database[id].sr.receiver}, {database[id].sr.sender, "*"} };
	for (int i = 0; i < 4; i++)
		Map[sr[i]].erase({id, database[id].date});

	mail_status[id] = 2;
	count_mail--;
	return count_mail;
}

void query(COMMAND& q, vector<int>& result)
{
	Sr_data sr;
	if (q._F == true)
		if (q._T == true)
		{
			transform(q._from.begin(), q._from.end(), q._from.begin(), ::tolower);
			transform(q._to.begin(), q._to.end(), q._to.begin(), ::tolower);
			sr = {q._from, q._to};
		}
		else
		{
			transform(q._from.begin(), q._from.end(), q._from.begin(), ::tolower);
			sr = {q._from, "*"};
		}
	else
		if (q._T == true)
		{
			transform(q._to.begin(), q._to.end(), q._to.begin(), ::tolower);
			sr = {"*", q._to};
		}
		else
		{
			sr = {"*", "*"};
		}

	set<IDandDate>::iterator it;
	for (it = Map[sr].lower_bound({0, q._date[0]}); it != Map[sr].lower_bound({10001, q._date[1]}); it++)
	{
		stack<bool> s;
		for (int i = 0; i < q._keyword.size(); i++)
		{
			if (q._keyword[i] != "!" and q._keyword[i] != "&" and q._keyword[i] != "|")
				if (database[it->id].keyword.find(q._keyword[i]) == database[it->id].keyword.end())
					s.push(false);
				else
					s.push(true);
			else if (q._keyword[i] == "!")
				s.top() = !s.top();
			else
			{
				bool con1 = s.top();
				s.pop();
				bool con2 = s.top();
				s.pop();
				if (q._keyword[i] == "&")
					s.push(con1 and con2);
				else
					s.push(con1 or con2);
			}
		}

		if (s.top() == true)
			result.push_back(it->id);
	}

	sort(result.begin(), result.end());
	return;
}

/*
int main()
{
	char command[10];
	while (scanf("%s", command) != EOF)
	{
		switch (command[0])
		{
			case 'a':
				char filename[100];
				scanf("%s", filename);
				add(filename);
				break;

			case 'r':
				int id;
				remove(id);
				break;

			case 'q':
				char q[600];
				fgets(q, 600, stdin);
				query(q);
		}
	}

	return 0;
}
*/

