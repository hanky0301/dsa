#include <iostream>
#include <algorithm>
#include <stdio.h>
using namespace std;

template <class T>
int GCD_By_Def(const T& a, const T& b, int& count)
{
	int ans = 1;
	count = 0;
	for (int i = 2; i <= min(a, b); i++)
	{
		count++;
		if (a % i == 0 && b % i == 0)
			ans = i;
	}
	return ans;
}

template <class T>
int GCD_By_Reverse_Search(const T& a, const T& b, int& count)
{
	count = 0;
	for (int i = min(a, b); i >= 1; i--)
	{
		count++;
		if (a % i == 0 && b % i == 0)
			return i;
	}
}

template <class T>
int GCD_By_Binary(const T& a, const T& b, int& count)
{
	int n = min(a, b);
	int m = max(a, b);
	int ans = 1;
	count = 0;

	while (n != 0 && m != 0)
	{
		count++;
		if (n % 2 == 0 && m % 2 == 0)
		{
			ans *= 2;
			n /= 2;
			m /= 2;
		}
		else if(n % 2 == 0 && m % 2 != 0)
			n /= 2;
		else if (n % 2 != 0 && m % 2 == 0)
			m /= 2;
		if (n > m)
			swap(n, m);

		m -= n;
	}
	return n * ans;
}

template <class T>
int GCD_By_Euclid(const T& a, const T& b, int& count)
{
	int n = min(a, b);
	int m = max(a, b);
	int tmp;
	count = 0;
	
	while (m % n != 0)
	{
		count++;
		tmp = n;
		n = m % n;
		m = tmp;
	}
	return n;
}

int main()
{
	int a, b;
	int count;
	while (1)
	{
		scanf("%d", &a);
		if (a == 0)
			break;
		scanf("%d", &b);
		printf("Case (%d, %d): GCD-By-Def = %d, ", a, b, GCD_By_Def(a, b, count));
		printf("taking %d iterations\n", count);
			
		printf("Case (%d, %d): GCD-By-Reverse-Search = %d, ", a, b, GCD_By_Reverse_Search(a, b, count));
		printf("taking %d iterations\n", count);

		printf("Case (%d, %d): GCD-By-Binary = %d, ", a, b, GCD_By_Binary(a, b, count));
		printf("taking %d iterations\n", count);

		printf("Case (%d, %d): GCD-By-Euclid = %d, ", a, b, GCD_By_Euclid(a, b, count));
		printf("taking %d iterations\n", count);
	}
	return 0;
}
