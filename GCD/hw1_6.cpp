#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
using namespace std;

void reverse(int n[], int length)
{
	int tmp;
	for (int i = 0; i < length / 2; i++)
	{
		tmp = n[i];
		n[i] = n[length-1-i];
		n[length-1-i] = tmp;
	}
	return;
}

void dataToNum(char data[], int num[], int& length)
{
	for (int i = 0; i < strlen(data); i++)
		num[i] = data[i] - '0';
	length = strlen(data);
	return;
}

class bigInt
{
	public:
		int num[110];
		int length;

		bigInt& operator=(const bigInt& bigN)
		{
			for (int i = 0; i < 110; i++)
				num[i] = bigN.num[i];
			length = bigN.length;
		}
		bigInt& operator=(int intN)
		{
			if (intN == 1)
				num[0] = 1;
			length = 1;
			return *this;
		}
		bool operator!=(const int& intN) const
		{
			if (intN == 0)
				return (num[0] != 0);
		}

		bool operator<(const bigInt& bigN) const
		{
			int i;
			if (length != bigN.length)
				return (length < bigN.length);
			else
			{
				for (i = 0; i < length; i++)
					if (num[i] != bigN.num[i])
						return (num[i] < bigN.num[i]);
				if (i == length)
					return false;
			}
		}
		
		int operator%(int intN) const
		{
			if (intN == 2)
				return (num[length-1] % 2);
		}

		bigInt& operator*=(int intN)
		{
			if (intN == 2)
			{
				reverse(num, length);
				for (int i = 0; i < 5; i++)
					num[i] *= 2;
				for (int i = 0; i < length; i++)
				{
					num[i+1] += num[i] / 10;
					num[i] %= 10;
				}
				if (num[length] == 1)
					length++;
				reverse(num, length);
			}
			return *this;
		}
		bigInt& operator/=(int intN)
		{
			if (intN == 2)
			{
				for (int i = 0; i < length; i++)
				{
					if (num[i] % 2 != 0 && i != length - 1)
						num[i+1] += 10;
					num[i] /= 2;
				}
				if (num[0] == 0)
				{
					for (int i = 0; i < length; i++)
						num[i] = num[i+1];
					num[length-1] = 0;
					length--;
				}
				return *this;
			}
		}
		bigInt& operator-=(bigInt& bigN)
		{
			reverse(num, length);
			reverse(bigN.num, bigN.length);
			for (int i = 0; i < length; i++)
				num[i] -= bigN.num[i];
			for (int i = 0; i < length; i++)
				if (num[i] < 0)
				{
					num[i+1]--;
					num[i] += 10;
				}
			for (int i = 109; i >= 0; i--)
				if (num[i] != 0)
				{	
					length = i + 1;
					break;
				}
			reverse(num, length);
			reverse(bigN.num, bigN.length);
			return *this;
		}
};

template <class T>
T GCD_By_Binary(const T& a, const T& b)
{
	T n = min(a, b);
	T m = max(a, b);
	int ans = 0;
	
	while (n != 0 && m != 0)
	{
		if (n % 2 == 0 && m % 2 == 0)
		{
			ans++;
			n /= 2;
			m /= 2;
		}
		else if (n % 2 == 0 && m % 2 != 0)
			n /= 2;
		else if (n % 2 != 0 && m % 2 == 0)
			m /= 2;

		if (m < n)
			swap(n, m);
		m -= n;
	}
	for (int i = 0; i < ans; i++)
		n *= 2;
	return n;
}

int main()
{
	char data1[110];
	char data2[110];
	bigInt a = {{0}, 0};
	bigInt b = {{0}, 0};
	bigInt gcd;
	
	scanf("%s", data1);
	scanf("%s", data2);

	dataToNum(data1, a.num, a.length);
	dataToNum(data2, b.num, b.length);
	gcd = GCD_By_Binary(a, b);
	
	for (int i = 0; i < gcd.length; i++)
		printf("%d", gcd.num[i]);
	printf("\n");
	
	return 0;
}
